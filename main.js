var clicks = 0; //main click variable
var CPC = 1; //Clicks per Click
var CPS = 0; //clicks per seconds
var up1cost = 30; //cost for upgrade 1
var up1count = 0; //upgrade 1 count
var upex1 = 10; //exponential value 
var up2cost = 120; //cost for upgrade 2
var up2count = 0; //upgrade 2 count
var upex2 = 10; //exponential value
var up3cost = 660; //cost for upgrade 2
var up3count = 0; //upgrade 2 count
var upex3 = 10; //exponential value

//Toastr options
toastr.options = {
  "closeButton": false,
  "debug": false,
  "positionClass": "toast-bottom-full-width",
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "2000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
//end Toastr options

 function btnClick(){
    clicks = clicks + CPC; //increases clicks by CPC	 
    $("span.timesClicked").html(clicks.toFixed(0)) //parses over via jquery
    checkAll() 	
    return true;  
}
function up1buy(){
	upex1 = up1cost/3 * 3.5
	clicks = clicks - up1cost;
	up1count = up1count + 1;
	up1cost = upex1;
	CPS = CPS + 0.1;
	$("span.timesClicked").html(clicks.toFixed(0))
	$("#up1count").html(up1count.toFixed(0))
	$("#up1cost").html(up1cost.toFixed(0))	
	$("#cps").html(CPS.toFixed(1))
	toastr.clear();
	toastr.info("You bought a mousepad!");
	checkAll()
}
function up2buy(){
	upex2 = up2cost/3 * 4
	clicks = clicks - up2cost;
	up2count = up2count + 1;
	up2cost = upex2;
	CPS = CPS + 1;
	$("span.timesClicked").html(clicks.toFixed(0))
	$("#up2count").html(up2count.toFixed(0))
	$("#up2cost").html(up2cost.toFixed(0))	
	$("#cps").html(CPS.toFixed(1))
	toastr.clear();
	toastr.info("You bought a mouse!");
	checkAll() 
}
function up3buy(){
	upex3 = up3cost/3 * 4
	clicks = clicks - up3cost;
	up2count = up3count + 1;
	up2cost = upex3;
	CPS = CPS + 10;
	$("span.timesClicked").html(clicks.toFixed(0))
	$("#up3count").html(up3count.toFixed(0))
	$("#up3cost").html(up3cost.toFixed(0))	
	$("#cps").html(CPS.toFixed(1))
	toastr.clear();
	toastr.info("You bought a spacebar!");
	checkAll() 
}

function autoCPS(){
	clicks = clicks + CPS;
	$("span.timesClicked").html(clicks.toFixed(0))
	checkAll()
}

function checkAll(){
	if(clicks < up1cost){
		$("#up1bt").addClass("disabled")
	} else {
		$("#up1bt").removeClass("disabled")
	} 
	if(clicks < up2cost){
		$("#up2bt").addClass("disabled")
	} else {
		$("#up2bt").removeClass("disabled")
	} 
	if(clicks < up3cost){
		$("#up3bt").addClass("disabled")
	} else {
		$("#up3bt").removeClass("disabled")
	} 
}

function checkOne(intg){
	if(clicks < intg){
		$("#up2bt").addClass("disabled")
	} else {
		$("#up2bt").removeClass("disabled")
	} 
}